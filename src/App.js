import { LoginPage } from './LoginPage/index'
import { ShoppingCartPage } from './Cart/index'
import { ItemsListPage } from './ItemsList/index'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { configureStore } from '@reduxjs/toolkit';
import { cartSlice } from './StateManagement/CartItemsReducer';
import { Provider } from 'react-redux';


function App() {
  const store = configureStore({ reducer: { cart: cartSlice.reducer, } });
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route index element={<LoginPage />} />
          <Route path='/cart' element={<ShoppingCartPage />} />
          <Route path='/items' element={<ItemsListPage />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
