import { createSlice } from "@reduxjs/toolkit"

export const cartSlice = createSlice({
    name: 'cart',
    initialState: { items: [], sum: 0 },
    reducers: {
        recalculate: (state, value) => {
            const items = value.payload.selectedItems.items.filter(i => i.id !== value.payload.item.id);
            items.push(value.payload.item);
            const arr = Object.values(items).reduce((t, { price, value }) => t + (price * Number(value)), 0)
            return { items: items, sum: arr };
        },
    }
});

export const { recalculate } = cartSlice.actions;
