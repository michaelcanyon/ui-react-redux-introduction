import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { recalculate } from '../StateManagement/CartItemsReducer';
import { connect, ReactReduxContext } from 'react-redux';
import './styles.css'
import 'bootstrap/dist/css/bootstrap.min.css';

const mapStateToProps = (state) => ({
    cartSum: state.cart.sum
})

const ShoppingCart = (props) => {
    const navigate = useNavigate();
    const { store } = useContext(ReactReduxContext)

    return (<div className="container padding-bottom-3x mb-1">
        <div className="table-responsive shopping-cart">
            <table className="table">
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th className="text-center">Quantity</th>
                        <th className="text-center">Subtotal</th>
                        <th className="text-center">Discount</th>
                        <th className="text-center"><a className="btn btn-sm btn-outline-danger" href="#">Clear Cart</a></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div className="product-item">
                                <a className="product-thumb" href="#"><img src="https://www.bootdey.com/image/220x180/FF0000/000000" alt="Product" /></a>
                                <div className="product-info">
                                    <h4 className="product-title"><a href="#">Unionbay Park</a></h4><span><em>Size:</em> 10.5</span><span><em>Color:</em> Dark Blue</span>
                                </div>
                            </div>
                        </td>
                        <td className="text-center">
                            <div className="count-input">
                                <select className="form-control" onChange={(e) => {
                                    store.dispatch(recalculate({ item: { id: 1, value: e.target.value, price: 43.9 }, selectedItems: store.getState().cart }))
                                }}>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </td>
                        <td className="text-center text-lg text-medium">$43.90</td>
                        <td className="text-center text-lg text-medium">$18.00</td>
                        <td className="text-center"><a className="remove-from-cart" href="#" data-toggle="tooltip" title="" data-original-title="Remove item"><i className="fa fa-trash"></i></a></td>
                    </tr>
                    <tr>
                        <td>
                            <div className="product-item">
                                <a className="product-thumb" href="#"><img src="https://www.bootdey.com/image/220x180/5F9EA0/000000" alt="Product" /></a>
                                <div className="product-info">
                                    <h4 className="product-title"><a href="#">Daily Fabric Cap</a></h4><span><em>Size:</em> XL</span><span><em>Color:</em> Black</span>
                                </div>
                            </div>
                        </td>
                        <td className="text-center">
                            <div className="count-input">
                                <select className="form-control" onChange={(e) => store.dispatch(recalculate({ item: { id: 2, value: e.target.value, price: 24.89 }, selectedItems: store.getState().cart }))}>
                                    <option>1</option>
                                    <option selected="">2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </td>
                        <td className="text-center text-lg text-medium">$24.89</td>
                        <td className="text-center">—</td>
                        <td className="text-center"><a className="remove-from-cart" href="#" data-toggle="tooltip" title="" data-original-title="Remove item"><i className="fa fa-trash"></i></a></td>
                    </tr>
                    <tr>
                        <td>
                            <div className="product-item">
                                <a className="product-thumb" href="#"><img src="https://www.bootdey.com/image/220x180/9932CC/000000" alt="Product" /></a>
                                <div className="product-info">
                                    <h4 className="product-title"><a href="#">Cole Haan Crossbody</a></h4><span><em>Size:</em> -</span><span><em>Color:</em> Turquoise</span>
                                </div>
                            </div>
                        </td>
                        <td className="text-center">
                            <div className="count-input">
                                <select className="form-control" onChange={(e) => store.dispatch(recalculate({ item: { id: 3, value: e.target.value, price: 200 }, selectedItems: store.getState().cart }))}>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </td>
                        <td className="text-center text-lg text-medium">$200.00</td>
                        <td className="text-center">—</td>
                        <td className="text-center"><a className="remove-from-cart" href="#" data-toggle="tooltip" title="" data-original-title="Remove item"><i className="fa fa-trash"></i></a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div className="shopping-cart-footer">
            <div className="column">
                <form className="coupon-form" method="post">
                    <input className="form-control form-control-sm" type="text" placeholder="Coupon code" required="" />
                    <button className="btn btn-outline-primary btn-sm" type="submit">Apply Coupon</button>
                </form>
            </div>
            <div className="column text-lg">Subtotal: <span className="text-medium">${props.cartSum}</span></div>
        </div>
        <div className="shopping-cart-footer">
            <div className="column"><a className="btn btn-outline-secondary" onClick={() => navigate(-1)}><i className="icon-arrow-left"></i>&nbsp;Back to Shopping</a></div>
            <div className="column"><a className="btn btn-primary" href="#" data-toast="" data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Your cart" data-toast-message="is updated successfully!">Update Cart</a><a className="btn btn-success" href="#">Checkout</a></div>
        </div>
    </div>)

}

export const ShoppingCartPage = connect(mapStateToProps)(ShoppingCart)