import React, { useState, useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import './styles.css'

export const LoginPage = () => {
    const navigate = useNavigate();
    const [credentials, setCredentials] = useState({ login: null, email: null });
    const confirmData = () => navigate('/items')

    return (
        <div id="login-form-wrap">
            <h2>Login</h2>
            <div id="login-form">
                <p>
                    <input
                        type="text"
                        id="username"
                        name="username"
                        placeholder="Username"
                        onChange={(e) => setCredentials((prevState) => ({ ...prevState, login: e.target.value }))}
                        required />
                    <i class="validation">
                        <span></span>
                        <span></span>
                    </i>
                </p>
                <p>
                    <input
                        type="email"
                        id="email"
                        name="email"
                        placeholder="Email Address"
                        onChange={(e) => setCredentials((prevState) => ({ ...prevState, email: e.target.value }))}
                        required />
                    <i class="validation">
                        <span></span>
                        <span></span>
                    </i>
                </p>
                <p>
                    <input type="submit" id="login" value="Login" onClick={confirmData} />
                </p>
            </div>
            <div id="create-account-wrap">
                <p>Not a member? <a href="#">Create Account</a></p>
            </div>
        </div>
    )
}